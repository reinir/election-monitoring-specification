# Tes kemampuan backend

Ini adalah tes untuk menguji kemampuan backend.
Tugas anda:
1. Buatlah Rest API server yang memiliki 3 fungsi
2. Buatlah script untuk menjalankan server
3. Upload server yang sudah anda buat ke repository git baru

## Penjelasan Tugas 1

Buatlah Rest API server yang memiliki 3 fungsi, yaitu:

1. simpan data dengan endpoint http://localhost:6006/push
2. ambil data dengan endpoint http://localhost:6006/pull
3. hapus data dengan endpoint http://localhost:6006/clear

Spesifikasi detail ada di file Specification.java yang berisi 10 poin yang harus dipenuhi.

## Penjelasan Tugas 2

Buatlah script berisi semua perintah yang diperlukan untuk menjalankan server yang anda buat,
termasuk perintah untuk menyiapkan database dan perintah-perintah lainnya jika perlu.

Pastikan server yang anda buat dapat dijalankan dengan perintah berikut pada command prompt (shell):

```
mulai
```

atau

```
./mulai
```

## Penjelasan Tugas 3

Upload server yang sudah anda buat ke repository git baru.

Untuk memastikan server anda bisa dijalankan, lakukan langkah-langkah berikut:
- clone repository tersebut di folder lain
- jalankan "mulai" atau "./mulai" di folder tersebut
- jalankan "gradlew test" atau "./gradlew test" di folder project ini

# Penggunaan

Project ini menguji fungsi server yang anda buat,
dalam bahasa pemrograman yang sudah disepakati,
dengan cara mengirimkan request ke endpoint terkait dan memeriksa response-nya.

Pastikan anda sudah menginstall Java Development Kit (JDK).

Lalu jalankan:

```shell
gradlew test
```

atau

```shell
./gradlew test
```

Perhatikan pesan yang muncul:

```shell
Specification > flow_CLEAR_PUSH_PUSH_PULL_CLEAR_PULL() FAILED
    Expected: 200
    Actual  : 0
    1. HTTP status clear_url
```

Pesan di atas menunjukkan poin 1 belum terpenuhi.
Lihat file spesifikasi untuk mengetahui detailnya.
Lanjutkan pembuatan server hingga semua poin terpenuhi.

Jika hasil FAILED, perbaiki fungsi pada server yang anda buat.
Jika hasil sukses, berarti fungsi sudah sesuai spesifikasi.

Selamat bekerja dan semoga berhasil.

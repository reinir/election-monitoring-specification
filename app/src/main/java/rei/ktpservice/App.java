package rei.ktpservice;

public class App {
    public static void main(String[] args) {
        System.out.println("-------------------------");
        System.out.println("Automated Acceptance Test");
        System.out.println("-------------------------");
        System.out.println("");
        System.out.println("Instruction: call \"gradlew test\" or \"./gradlew test\" depending on your operating system or shell");
    }
}

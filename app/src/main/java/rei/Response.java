package rei;

import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.google.gson.JsonParser;

/**
 * @author Rei
 */
public class Response {

    protected Request fetch;
    public int status;
    public byte[] bytes;

    public Response(Request fetch) {
        this.fetch = fetch;
        try {
            status = fetch.connection.getResponseCode();
            if (status < 300) {
                try (var stream = new DataInputStream(fetch.connection.getInputStream())) {
                    bytes = stream.readAllBytes();
                } catch (IOException e) {
                }
            } else {
                try (var stream = new DataInputStream(fetch.connection.getErrorStream())) {
                    bytes = stream.readAllBytes();
                } catch (IOException e) {
                }
            }
        } catch (IOException e) {
            status = 0;
        }
    }

    public String json() {
        try {
            return JsonParser.parseString(string()).toString();
        } catch (Exception e) {
            return null;
        }
    }

    public String string() {
        return string(StandardCharsets.UTF_8);
	}

    public String string(Charset charset) {
        try {
            return new String(bytes, charset);
        } catch (Exception e) {
            return null;
        }
    }

    public String string(String charset) {
        try {
            return new String(bytes, charset);
        } catch (Exception e) {
            return null;
        }
    }

}

package rei;

/**
 * @author Rei
 */
public class MyAssertion extends AssertionError {
    protected String message;

    protected MyAssertion(String message) {
        super();
        this.message = message;
    }

    protected MyAssertion(String name, String message) {
        super();
        this.message = message + '\n' + name;
    }

    protected MyAssertion remove(int fromHead, int fromTail) {
        var st = getStackTrace();
        setStackTrace(java.util.Arrays.copyOfRange(st, fromHead, st.length - fromTail));
        return this;
    }

    @Override
    public String toString() {
        return message;
    }

    public static void expect_toEqual(Object expected, Object actual) {
        if (expected == null) throw new MyAssertion("Expected is null").remove(1, 0);
        if (!expected.equals(actual)) throw new MyAssertion("Expected: " + expected + " \nActual  : " + actual + " ").remove(1, 0);
    }

    public static void expect_toEqual(Object expected, Object actual, String name) {
        if (expected == null) throw new MyAssertion("Expected is null").remove(1, 0);
        if (!expected.equals(actual)) throw new MyAssertion(name, "Expected: " + expected + " \nActual  : " + actual + " ").remove(1, 0);
    }

    public static void expect(String message) {
        throw new MyAssertion(message).remove(1, 0);
    }
}
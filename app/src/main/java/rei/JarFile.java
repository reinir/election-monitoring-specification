package rei;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.google.gson.JsonParser;

/**
 * Access file in the jar.
 * @author Rei
 */
public class JarFile {
    protected Charset charset = StandardCharsets.UTF_8;
    protected String name;

    /**
     * Specify the file name. Use absolute path.
     * @param __ The file name
     * @return ResourceFile object
     */
    public static JarFile name(String __) {
        return new JarFile(__);
    }

    protected JarFile(String name) {
        this.name = name;
    }

    public JarFile charset(Charset charset) {
        this.charset = charset;
        return this;
    }

    public JarFile charset(String charset) {
        this.charset = Charset.forName(charset, this.charset);
        return this;
    }

    public boolean exists() {
        try (var stream = JarFile.class.getResourceAsStream(name)) {
            return stream != null;
        } catch (Exception e) {
            return false;
        }
    }

    public byte[] read() {
        try (var stream = JarFile.class.getResourceAsStream(name)) {
            return stream.readAllBytes();
        } catch (Exception e) {
            return null;
        }
    }

    public String readString() {
        try (var stream = JarFile.class.getResourceAsStream(name)) {
            return new String(stream.readAllBytes(), charset);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return String of the JSON content with whitespaces removed, null if file doesn't exist or doesn't contain JSON.
     */
    public String readJson() {
        try (var stream = JarFile.class.getResourceAsStream(name)) {
            return JsonParser.parseString(new String(stream.readAllBytes(), charset)).toString();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean write(byte[] data) {
        return false;
    }

    public boolean write(String data) {
        return false;
    }
}

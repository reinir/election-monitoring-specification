package rei;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * @author Rei
 */
public class Request {
    private static final Logger LOG = Logger.getLogger("rei");
    public int status;
    public byte[] bytes;
    public URL url;
    public HttpURLConnection connection;
    public String body = null;

    public static Request url(String string) {
        return new Request(string);
    }

    public Request(String string) {
        try {
            url = new URL(string);
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            LOG.warning(e.toString());
        }
    }

    public Request header(String key, String value) {
        if (connection != null) {
            connection.setRequestProperty(key, value);
        }
        return this;
    }

    public Request method(String method) {
        if (connection != null) {
            try {
                connection.setRequestMethod(method);
            } catch (ProtocolException e) {
                LOG.warning(e.toString());
            }
        }
        return this;
    }

    public Request body(String body) {
        this.body = body;
        return this;
    }

    public Response fetch() {
        if (body != null) {
            if (connection != null) {
                connection.setDoOutput(true);
                try (DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {
                    stream.writeBytes(body);
                    stream.flush();
                } catch (IOException e) {
                    LOG.warning(e.toString());
                }
            }
        }
        return new Response(this);
    }

    public Response fetch(String method) {
        method(method);
        if (body != null) {
            if (connection != null) {
                connection.setDoOutput(true);
                try (DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {
                    stream.writeBytes(body);
                    stream.flush();
                } catch (IOException e) {
                    LOG.warning(e.toString());
                }
            }
        }
        return new Response(this);
    }

    public Response send(String method) {
        method(method);
        if (body != null) {
            if (connection != null) {
                connection.setDoOutput(true);
                try (DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {
                    stream.writeBytes(body);
                    stream.flush();
                } catch (IOException e) {
                    LOG.warning(e.toString());
                }
            }
        }
        return new Response(this);
    }

    public Response send() {
        if (body != null) {
            if (connection != null) {
                connection.setDoOutput(true);
                try (DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {
                    stream.writeBytes(body);
                    stream.flush();
                } catch (IOException e) {
                    LOG.warning(e.toString());
                }
            }
        }
        return new Response(this);
    }

}

package rei;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * Access file in the local filesystem.
 * @author Rei
 */
public class File {
    protected Charset charset = StandardCharsets.UTF_8;
    protected Path path;

    /**
     * Specify the file name. Use absolute path.
     * @param __ The file name
     * @return File object
     */
    public static File name(String __) {
        return new File(__);
    }

    protected File(String name) {
        this.path = Path.of(name);
    }

    public File charset(Charset charset) {
        this.charset = charset;
        return this;
    }

    public File charset(String charset) {
        this.charset = Charset.forName(charset, this.charset);
        return this;
    }

    public boolean exists() {
        return path.toFile().exists();
    }

    public byte[] read() {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            return null;
        }
    }

    public String readString() {
        try {
            return new String(Files.readAllBytes(path), charset);
        } catch (IOException e) {
            return null;
        }
    }

    public String readJson() {
        try {
            return JsonParser.parseString(new String(Files.readAllBytes(path), charset)).toString();
        } catch (IOException | JsonSyntaxException e) {
            return null;
        }
    }

    public boolean write(byte[] data) {
        try {
            Files.write(path, data);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean write(String data) {
        return write(data.getBytes(charset));
    }
}

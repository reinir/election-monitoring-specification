package rei;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rei
 */
public class Configuration {
    protected static String namespace = null;
    protected static final Map<String, String> PARAMETERS = new HashMap<>();
    protected static final Map<String, String> ARGUMENTS = new HashMap<>();

    static {
    }

    public static void namespace(String name) {
        namespace = name;
    }
    public static void set(String name, String value) {
        ARGUMENTS.put(name, value);
    }

    public static void setDefault(String name, String value) {
        PARAMETERS.put(name, value);
    }

    /**
     * Get parameter value.
     * 
     * @param name
     * @return The value of the specified parameter name, null if not found
     */
    public static String get(String name) {
        return get(name, null);
    }

    /**
     * Get parameter value.
     *
     * @param name
     * @param defaultValue
     * @return The value of the specified parameter name, defaultValue if not found
     */
    public static String get(String name, String defaultValue) {
        //from command line arguments
        String value = ARGUMENTS.get(namespace != null ? (namespace + "." + name) : name);
        if (value != null) return value;

        //from environment variables
        value = System.getenv(namespace != null ? (namespace + "." + name) : name);
        if (value != null) return value;

        //from in-process parameters
        value = PARAMETERS.get(name);
        if (value != null) return value;

        return defaultValue;
    }
    
    /**
     * Parse command line arguments.
     * <p>
     * Usage:
     * </p>
     * <pre>
     * public static void main(String[] args) {
     *     Configuration.parse(args);
     *     //your code here
     * }
     * </pre>
     * 
     * @param args
     */
    public static void parse(String[] args) {
        for (String arg : args) {
            if (arg.startsWith("--")) {
                String[] parts = arg.substring(2).split("=", 2);
                if (parts.length == 2) {
                    ARGUMENTS.put(parts[0], parts[1]);
                }
            }
        }
    }
}

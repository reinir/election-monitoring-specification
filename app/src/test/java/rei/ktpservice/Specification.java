package rei.ktpservice;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import rei.Configuration;
import rei.MyAssertion;
import rei.Request;
import rei.JarFile;

/**
 * @author Rei
 */
public class Specification {
    String push_url = Configuration.get("push_url");
    String pull_url = Configuration.get("pull_url");
    String clear_url = Configuration.get("clear_url");

    @BeforeAll
    static void initialization() {
        Configuration.namespace("ktpservice");
        Configuration.setDefault("push_url", "http://localhost:6006/push");     // simpan data
        Configuration.setDefault("pull_url", "http://localhost:6006/pull");     // ambil data
        Configuration.setDefault("clear_url", "http://localhost:6006/clear");   // hapus data
    }

    @Test
    void flow_CLEAR_PUSH_PUSH_PULL_CLEAR_PULL() {
        // GIVEN
        String ktp1 = JarFile.name("/ktp1.json").readJson();
        String ktp2 = JarFile.name("/ktp2.json").readJson();
        String pullResponseJson3 = JarFile.name("/long_pull_response3.json").readJson();
        String pullResponseJson0 = "[]";

        // WHEN
        var clearResponse1 = Request.url(clear_url).send("POST");
        var pushResponse1 = Request.url(push_url).body(ktp1).send("POST");
        var pushResponse2 = Request.url(push_url).body(ktp2).send("POST");
        var pullResponse3 = Request.url(pull_url).send();
        var clearResponse2 = Request.url(clear_url).send("POST");
        var pullResponse0 = Request.url(pull_url).send();

        // THEN
        MyAssertion.expect_toEqual(200, clearResponse1.status, "1. HTTP status clear_url");
        MyAssertion.expect_toEqual(200, pushResponse1.status, "2. HTTP status push_url");
        MyAssertion.expect_toEqual(ktp1, pushResponse1.json(), "3. dari push_url");
        MyAssertion.expect_toEqual(200, pushResponse2.status, "4. HTTP status push_url");
        MyAssertion.expect_toEqual(ktp2, pushResponse2.json(), "5. dari push_url");
        MyAssertion.expect_toEqual(200, pullResponse3.status, "6. HTTP status pull_url");
        MyAssertion.expect_toEqual(pullResponseJson3, pullResponse3.json(), "7. dari pull_url");
        MyAssertion.expect_toEqual(200, clearResponse2.status, "8. HTTP status clear_url");
        MyAssertion.expect_toEqual(200, pullResponse0.status, "9. HTTP status pull_url");
        MyAssertion.expect_toEqual(pullResponseJson0, pullResponse0.json(), "10. dari pull_url");
    }

}
